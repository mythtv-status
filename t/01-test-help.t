#!usr/bin/perl

use Test::More tests => 2;
use FindBin qw/$Bin/;

# Test that the blurb of how to get help is printed.
my $out = `$Bin/../bin/mythtv-status --zzz 2>&1`;

my $expected = <<EOF;
Unknown option: zzz

Use --help for help.

Usage:
     mythtv-status [options]

EOF

ok($out eq $expected, 'Invalid option returns help blurb');

# Check the help output, using --help.
$out = `$Bin/../bin/mythtv-status --help 2>&1`;

$expected = <<EOF;
Usage:
     mythtv-status [options]

Options:
    -c, --colour
        Use colour when showing the status of the encoder(s).

    --date
        Set the date to run as, used for debugging purposes.

    -d, --description
        Display the description for the scheduled recordings.

    --disk-space-warn
        The threshold (in percent) of used disk space that we should show
        the disk space in red (if using colour) or send an email if we're in
        email mode with email only on warnings.

    --encoder-details
        Display the input ID and channel name against the recording details.

    --encoder-skip-idle
        Suppress displaying idle encoders in the Encoders block.

    --encoder-warn-non-idle
        Display warnings if an encoder is not idle. This is the default, it
        allows you to know if an encoder or the MythTV system is busy. To
        disable, use --no-encoder-warn-non-idle.

    -e, --episode
        Display the episode (subtitle) for the scheduled recordings.

    --email <address>[ --email <address> ...]
        Send the output to the listed email addresses. By default the
        encoder status, currently recording shows and time till next
        recording is suppressed from the email.

        To turn the additional blocks on you can use --encoders,
        --recording-now and/or --next-recording.

        By default highlight is turned on, to disable it use --nohighlight.

    --email-only-on-alert
        Only send an email out (if --email is present) if there is an alert
        (i.e., schedule conflict or low disk space).

    -?, --help
        Display help.

    --file <file>
        Load XML from the file specified instead of querying a MythTV
        backend. Handy for debugging things.

    --save-file <file>
        Save the XML we received from the MythTV backend. Handy for
        debugging things.

    --guide-days-warn <days>
        Warn if the number of days of guide data present is equal to or
        below this level. Default is 2 days.

    -h HOST, --host=HOST
        The host to check, defaults to localhost.

    --highlight
        Surround any items that are considered a warning with asterisks.
        This helps to highlight an issue if colour mode is disabled.

    --nostatus, --noencoders, --norecording-now, --noscheduled-recordings,
    --noschedule-conflicts, --nonext-recording, --nototal-disk-space,
    --nodisk-space, --noguide-data, --noauto-expire
        Suppress displaying blocks of the output if they would normally be
        displayed.

    -p PORT, --port=PORT
        The port to use when connecting to MythTV, defaults to 6544.

    --oneliner-bullets
        Insert asterisks (*) before each of the oneliners to stop some email
        clients from thinking the oneliner block is a paragraph and trying
        to word wrap them.

    --auto-expire
        Display the shows due to auto expire (output is normally
        suppressed).

    --auto-expire-count
        How many of the auto expire shows to display, defaults to 10.

    --recording-in-warn
        If the "Next Recording In" time is less than this amount, display it
        in red. This in seconds, and defaults to 3600 (1 hour).

    --verbose
        Have slightly more verbose output. This includes any warnings that
        might be generated while parsing the XML.

    -v, --version
        Show the version of mythtv-status and then exit.

EOF

ok($out eq $expected, '--help generates help output');
