#!usr/bin/perl -w

use strict;
use Test::More tests => 3;
use FindBin qw/$Bin/;

my $xml = "$Bin/xml/wide-character.xml";

# Test that the guide data warning is present.
my $out = `$Bin/../bin/mythtv-status -d --file $xml --date '2012-07-07 22:12' 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
unlike($out, qr/Wide character in print/m, 'No warning about wide characters');
like($out, qr/measurements – with/,' Description contains wide character');
