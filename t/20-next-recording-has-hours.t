#!usr/bin/perl -w

# There was bug which converted:
#   Next Recording In: 10 Hours, 42 Minutes
# To:
#   Next Recording In: 142 Minutes
# Due to the regex for "0 Hours" being a bit enthusastic.


use strict;
use Test::More tests => 2;
use FindBin qw/$Bin/;

my $xml = "$Bin/xml/bug729400-wrong-recording-date.xml";

# Test that the guide data warning is present.
my $out = `$Bin/../bin/mythtv-status -d --file $xml --date '2013-12-09T21:02:12Z' 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/Next Recording In: 10 Hours, 42 Minutes/, 'Next recording has hours as required');
