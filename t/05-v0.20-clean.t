#!usr/bin/perl -w

use strict;
use Test::More tests => 42;
use FindBin qw/$Bin/;

my $xml = "$Bin/xml/single-filesytem-0.20.2.xml";

# Test that the blurb of how to get help is printed.
my $out = `$Bin/../bin/mythtv-status --file $xml 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/^Status as of\.*: Thu Nov 29 2007, 6:43 PM$/m, 'Status line is present');
like($out, qr/^Total Disk Space: Total space is 100 GB, with 72\.2 GB used \(72%\)$/m, 'Total disk space is present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present');
like($out, qr/^Encoders:\s+cerberus \(9\) - Recording$/m, 'Encoder is present, and recording');
like($out, qr/^Recording Now:\s+3 News \(TV3\), Ends: 19:00:00$/m, 'Recording now is present (the news)');

# Test that the status isn't present if --nostatus is passed.

$out = `$Bin/../bin/mythtv-status --file $xml --nostatus 2>&1`;

like($out, qr/MythTV status for localhost/, 'Header is present');
unlike($out, qr/^Status as of\.*:/m, 'Status line is not present');
like($out, qr/^Total Disk Space: Total space is 100 GB, with 72\.2 GB used \(72%\)$/m, 'Total disk space is present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present');
like($out, qr/^Encoders:\s+cerberus \(9\) - Recording$/m, 'Encoder is present, and recording');
like($out, qr/^Recording Now:\s+3 News \(TV3\), Ends: 19:00:00$/m, 'Recording now is present (the news)');

# Test that the disk space isn't present if --nototal-disk-space is passed.

$out = `$Bin/../bin/mythtv-status --file $xml --nototal-disk-space 2>&1`;

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/^Status as of\.*: Thu Nov 29 2007, 6:43 PM$/m, 'Status line is present');
unlike($out, qr/^Total Disk Space: Total space is 100 GB, with 72\.2 GB used \(72%\)$/m, 'Total disk space is not present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present');
like($out, qr/^Encoders:\s+cerberus \(9\) - Recording$/m, 'Encoder is present, and recording');
like($out, qr/^Recording Now:\s+3 News \(TV3\), Ends: 19:00:00$/m, 'Recording now is present (the news)');

# Test that the encoders details aren't present if --noencoders is passed.

$out = `$Bin/../bin/mythtv-status --file $xml --noencoders 2>&1`;

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/^Status as of\.*: Thu Nov 29 2007, 6:43 PM$/m, 'Status line is present');
like($out, qr/^Total Disk Space: Total space is 100 GB, with 72\.2 GB used \(72%\)$/m, 'Total disk space is present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present');
unlike($out, qr/^Encoders:\s+cerberus \(9\) - Recording$/m, 'Encoder is not present');
like($out, qr/^Recording Now:\s+3 News \(TV3\), Ends: 19:00:00$/m, 'Recording now is present (the news)');

# Test that the recording now details aren't present if --norecording-now is passed.

$out = `$Bin/../bin/mythtv-status --file $xml --norecording-now 2>&1`;

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/^Status as of\.*: Thu Nov 29 2007, 6:43 PM$/m, 'Status line is present');
like($out, qr/^Total Disk Space: Total space is 100 GB, with 72\.2 GB used \(72%\)$/m, 'Total disk space is present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present');
like($out, qr/^Encoders:\s+cerberus \(9\) - Recording$/m, 'Encoder is present, and recording');
unlike($out, qr/^Recording Now:\s+3 News \(TV3\) Ends: 19:00:00$/m, 'Recording now is not present');

# Test that the encoder details for a recording are present if --encoder-details is passed.

$out = `$Bin/../bin/mythtv-status --file $xml --encoder-details 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/^Status as of\.*: Thu Nov 29 2007, 6:43 PM$/m, 'Status line is present');
like($out, qr/^Total Disk Space: Total space is 100 GB, with 72\.2 GB used \(72%\)$/m, 'Total disk space is present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present');
like($out, qr/^Encoders:\s+cerberus \(9\) - Recording$/m, 'Encoder is present, and recording');
like($out, qr/^Recording Now:\s+3 News \(TV3\), Enc: 9, Chan: 3, Ends: 19:00:00$/m, 'Recording now is present (the news) with encoder details');

# Test that the encoder details for a recording are present if --encoder-details is passed.

$out = `$Bin/../bin/mythtv-status --file $xml --oneliner-bullets 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/^\* Status as of\.*: Thu Nov 29 2007, 6:43 PM$/m, 'Status line is present (with bullet)');
like($out, qr/^\* Total Disk Space: Total space is 100 GB, with 72\.2 GB used \(72%\)$/m, 'Total disk space is present (with bullet)');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present');
like($out, qr/^Encoders:\s+cerberus \(9\) - Recording$/m, 'Encoder is present, and recording');
like($out, qr/^Recording Now:\s+3 News \(TV3\), Ends: 19:00:00$/m, 'Recording now is present (the news)');
