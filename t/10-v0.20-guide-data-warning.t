#!usr/bin/perl -w

use strict;
use Test::More tests => 6;
use FindBin qw/$Bin/;

my $xml = "$Bin/xml/guide-data-1-day-0.20.xml";

# Test that the guide data warning is present.
my $out = `$Bin/../bin/mythtv-status --file $xml 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
like($out, qr/^Guide Data\.*: There is 1 days worth of data, through to 2007-12-30/m, 'Guide data warning is present');

# If --noguide-data is passed in, then the guide data warning should be suppressed.
$out = `$Bin/../bin/mythtv-status --file $xml --noguide-data 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present (turned off)');

# If --guide-days-warn is set to something different, like 0, then the guide data warning should be suppressed.
$out = `$Bin/../bin/mythtv-status --file $xml --guide-days-warn 0 2>&1`;

#diag($out);

like($out, qr/MythTV status for localhost/, 'Header is present');
unlike($out, qr/^Guide Data\.*:/m, 'Guide data warning is not present (warning level changed)');
