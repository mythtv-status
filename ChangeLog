2025-01-02  Andrew Ruthven
  Release version 1.2.0.

  There isn't really a default action for the Makefile, running the tests
  isn't really sensible.

2025-01-01  Andrew Ruthven
  Strip out Debian package logic from Makefile, now in a dedicated repo.

  Generate the files that need to find other parts of this package to for
  them to be installed in other places. This will hopefully simplify the
  packaging on distros other than Debian where things may be in other places
  than I expect.

  Install man pages in /usr/share/man as per FHS.

2023-08-01  Andrew Ruthven
  If mythtv-update-motd is run via interactive shell, always try to run,
  regardless of the flags.

  Provide, hopefully sane, defaults in health-check.

  Support the Fedora config file for: health-check, mythtv_recording_now,
  mythtv_recording_soon (not just mythtv-update-motd).

2023-07-27  Andrew Ruthven
  Install the systemd files into the UsrMerge location so we'll work with
  more distributions, thanks to Göran Uddeborg for pointing this out.

  If molly-guard or the Ubuntu/Debian style update-motd is present, install
  our hooks for those.

2023-07-27  Göran Uddeborg
  Install mythtv-update-motd

  Use a default for $HOST in all scripts.

2023-07-23  Andrew Ruthven
  Release version 1.1.0.

2023-07-16  Andrew Ruthven
  Remove bashisms

  Include my Makefile in the tarball, this allows using "make install" to install
  mythtv-status.

  Make all shell scripts much cleaner according to shellcheck.

  Refresh URLs in FAQ, update libmythtv-perl package name.

  Remove obsolete question/answer from FAQ as my patch for improving storage
  group data was merged in 2008.

  Update the "Why this tool?" section in the README.

  Actually install the mythtv-update-motd man page!

  Provide systemd service and timer files in tarball, hopefully useful as a
  starting point for distros other than Debian.

2001-02-14  Andrew Ruthven
  Release version 1.0.2.

2020-12-06  Andrew Ruthven
  Minor update to systemd service file.

  I noticed that if /run/motd.mythtv-status doesn't exist then
  update-motd.d/50-mythtv-status exits with exit code 1 and pam_motd spits a
  warning. I don't really want that to happen.

2019-01-29  Andrew Ruthven
  Release version 1.0.1.

2019-01-27  Andrew Ruthven
  Switching from using -w to use warnings, caused my previous method to
  disable warnings when calling MythTV::Program to break..

  The warning we're suppressing is:
  Argument "Default" isn't numeric in bitwise and (&) at /usr/share/perl5/MythTV/Program.pm line 95.

  Thanks to Göran Uddeborg for spotting this regression.

2019-01-11  Andrew Ruthven
  Add support for Fedora to mythtv-update-motd based on proposed file
  from Göran Uddeborg.

  mythtv-update-status now uses UPDATE_MOTD and UPDATE_MOTD_ARGS
  to replace RUN and ARGS respectively. Both RUN and ARGS will be
  used if the new variables aren't defined.

  Switch from /var/run to /run.

  Update the Copyright years.

  Release version 1.0.0.

2018-12-31  Andrew Ruthven
  Ship a systemd service file which correctly starts and stops the motd
  update services. As part of this change, if you use systemd or sysvinit
  to stop mythtv-status it will stop updating the motd snippet via cron.

2018-12-30  Andrew Ruthven
  Modify mythtv-update-motd to look to see if /etc/update-motd.d is
  present, if it is, then generate MOTD content in a compatible fashion.
  On Debian derived systems we now install a snippet into /etc/update-motd.d
  to cat the generated file if it is present.

2018-12-30  David Meritt
  Fix the output of Next Recording to never have negative numbers.

2018-12-09  Andrew Ruthven
  Traditionally I've considered any state which could cause you to lose
  recordings to be warning state. Some people don't want to be warned
  if an encoder is recording, so allow that with --no-encoder-warn-non-idle.

2018-12-07  Andrew Ruthven
  Tidy up conversion logic for disk sizes to fix a long running bug when
  the free space is converted to a different unit than the total space.

  Adjust threshold for when we switch to TB.

  Add an option --highlight which uses asterisk to highlight warnings.
  This is useful in emails to highlight your attention to the right place

2018-11-13  Andrew Ruthven
  Show device type in encoder output (i.e: DVB).
  Thanks to Eric Wilde for the suggestion and proposed patch.

  Eradicate tabs from the code.

2018-11-12  Eric Wilde
  Make Config::Auto an optional module.
  Minor re-arrangement of output to make it more consistent.
  Remove dependency on Sys::SigAction.

2017-01-26	Andrew Ruthven
	Update the Copyright years.
	Decode UTF-8 characters when we're getting data using MythTV::Program,
	thank you Illés Solfor submitting a patch to fix Schedule Conflicts.

	Release version 0.10.8.

2016-09-09	Andrew Ruthven
	Fix a bug where it wasn't possible to disable skipping idle
	encoders via the config file. Thanks to Karl for finding this.

2016-08-02	Andrew Ruthven
	Fix a long standing bug where if the Next Recording is a week
	and one day in the future, the Next Recording only shows
	the hours.

2016-08-02	Andrew Ruthven
	Somewhere between Date::Manip v6.0 and v6.32 the behaviour for
	DateCalc has changed, so that now a DateCalc will keep on
	increasing the hours field, without rolling over the days field.

	Switching to an approximate mode for DateCalc which resolves this.

2015-01-10	Andrew Ruthven
	Make the cron.d file for Debian use mythtv-update-motd to stop
	systemd from being upset.

	Release version 0.10.7.

2014-12-02	Andrew Ruthven
	Add in more UTF-8 encodings as required for Perl v5.18, thanks to
	Jan Schneider for reporting the UTF-8 issue.

	Release versions 0.10.5 & 0.10.6.

2014-09-11	Andrew Ruthven
	Spotted that if the next recording is in 30 hours, then it is
	displayed as "0 Days, 3". Correct it so it'll be displayed
	as "30 Hours". Also fixes an issue where "0 hours" was removed
	if the hour was a multiple of 10. Thanks to Stephan Seitz for
	reporting the multiple of 10 issue.

	Release version 0.10.4.

2013-10-28	Andrew Ruthven
        Guard against zero total disk space, this affected Werner Mahr.

	Fix up processing of dates with old style timestamps for in
	progress recordings.

	Add support to specify the date we're running mythtv-status on
	so that we can test various scenarios.

2013-10-27	Andrew Ruthven
	Handle the handling in POD in Perl 5.18. Force the return code
	=item lines to be handled correctly.

2013-02-09	Andrew Ruthven
	Update Jan's patch to be acceptable to non-US folks.  ;)

	Also process the timezone info for in progress recordings so the
	Ends time is correct.

2013-02-09	Jan Schneider
	Add support for processing the UTC time stamps for the schedule
	start time as well.

2013-01-29	Andrew Ruthven
	MythTV 0.26 now uses UTC in the time/date attributes in Status.
	We now convert it to the local timezone.

2012-07-07	Andrew Ruthven
	Switch to using the exact mode when working out the time to the
	next recording.  Also, if the next recording is in 1 Hour, 1 Minute
	don't report it as "1 Hour, 1 Minutes".  Thank you to Julian Gilbey
	for the patch fixing this.

	If the next recording is in $x Hours, 0 Minutes, remove the comma
	as well, previously it was outputting "$x Hours,".

	If the next recording is in more than 1 day, then switch to showing
	it in days and hours, for example "1 Day, 2 Hours".

	Hrmm, the XML protocol version was wrong for the encoder status,
	so we incorrectly showed "7" instead of "Recording" for an encoder
	that was in fact recording.

	Release version 0.10.2.

2012-07-06	Andrew Ruthven
	Correctly handle UTF-8 in the output from MythTV.

2012-06-14	Andrew Ruthven
	Setting $0 stopped --help and -? from working, fix that.

	Add the start of a test suite!

	Release versions 0.10.0 & 0.10.1.

2012-03-04	Andrew Ruthven
	It turns out that some email clients treat the oneliner block
	as a paragraph that absolutely most be wrapped.  Which causes
	rather ugly results.  Add a --oneliner-bullets option that
	puts asterisks at the start of each oneliner line to make them
	a bullet-ed list.

2012-01-18	Andrew Ruthven
	MythTV 0.25 now has the status info in a different location, look
	there.

2011-05-19	Andrew Ruthven
	It seems that MythTV has changed the ProtoVer field in
	0.23 to be a different version number.  Update some
	stanzas to use current version numbers.

2011-05-15      Andrew Ruthven
	My method of trying to ensure that we don't have long lived
	processes hanging around didn't work.  Now we fork a child
	to perform the query.

2011-03-03	Andrew Ruthven
	It turns out that $SIG{ALRM} doesn't always work with LWP::UserAgent
	as you'd expect.  I've certainly noticed mythtv-status hanging
	during cron jobs on a regular basis, and this could well explain
	it.

	Release version 0.9.6.

2010-12-01	Andrew Ruthven
	Make sure that Date::Manip is running in 5.x compatibility mode.

	Release version 0.9.5.

2010-09-16	Andrew Ruthven
	Add support for extra recording state.

	Be ready to switch to TB earlier.

2010-06-13	Andrew Ruthven
	Allow skipping idle encoders when showing the encoder status and
	make that the default.  Thanks to Ron Kellam for the suggestion.

	It seems that Date::Manip::DateCalc in some versions require that
	you parse in a ref for the error ref, passing in undef causes it
	to not perform the date calculation.  Parse in a ref, even if we
	don't actually check it.  Thanks to Ron Kellam for doing some
	leg work on tracking this issue down.

	Release version 0.9.4.

2010-04-29	Andrew Ruthven
	Handle the case where the disk space units returned from the server
	are different.  i.e., total is in GB and used is in MB.

	Fix my redirection of STDERR.

	Release version 0.9.4.

2009-05-22	Andrew Ruthven
	Only move /var/run/motd.new if it exists.

	Release version 0.9.3.

2009-04-11	Andrew Ruthven
	Show the encoder details for "Recording Now".
	Pass on that charset that MythTV gave us in any emails we send and
	set a sensible encoding.

2009-03-31	Andrew Ruthven
	Add support for the newer encoder statuses.

2009-02-20	Andrew Ruthven
	Allow using a YAML file for setting most of the command line flags.

2008-08-01	Andrew Ruthven
	Automatically convert the disk space units to more human readable
	forms if it is many GB.

2008-07-30	Andrew Ruthven
	Suppress the errors from XML::LibXML.

2008-07-11	Andrew Ruthven
	Relicense under the GPL v3.

	Release version 0.9.0.

2008-07-06	Andrew Ruthven
	Fix a couple of minor typos in the argument passing, and the help
	screen.  Thank you Mike Holden for reporting these.

2008-06-19	Andrew Ruthven
	Just extend the copyright time ranges to include 2008.

	Release version 0.8.1.

2008-06-18	Andrew Ruthven
	Modify the auto expire logic to reduce the amount of needless work
	that is done.  Thank you Tom Metro for point this out.

	Release version 0.8.0.

2008-05-28	Andrew Ruthven
	It turns out that secondary backends don't return total disk space,
	handle that in a nicer manner.  Thank you Steve Gunther for letting
	me know.

2008-05-23	Andrew Ruthven
	Add a timeout for the HTTP request to the backend.

2008-04-19	Andrew Ruthven
	Add a molly-guard check

2008-04-11	Andrew Ruthven
	Provide helper scripts that'll return true if the MythTV backend
	is:
	 * recording a show now, 
	 * will be recording one within the next hour (or is currently
	   recording)

2008-03-11	Andrew Ruthven
	Be less stressed about the XML protocol, let XPath matching do
	the validation for us.

	Release version 0.7.3.

2008-03-07	Andrew Ruthven
	The MythTV protocol on release-0-21-fixes has been increased to 39.

	Release version 0.7.2.
	
2008-01-24	Andrew Ruthven
	Release version 0.7.1.

2008-01-22	Andrew Ruthven
	Change the version boundaries for the disk space blocks to reflect
	current SVN.
	The backend returns the disk usage in MB, not GB.
	Optionally show the input ID and channel number for recordings.

2008-01-20	Andrew Ruthven
	Show the shows that will be deleted by the auto expirer, and their
	order.

	Release version 0.7.0

2007-12-26	Andrew Ruthven
	Release version 0.6.2
	
2007-12-25	Andrew Ruthven
	Conflicts should show the channel name as well.

2007-12-13	Andrew Ruthven
	Warn if there isn't enough guide data present.
	Show the channel name next to program details.

	Release version 0.6.1
	
2007-12-12	Andrew Ruthven
	Release version 0.6.0

2007-12-08	Andrew Ruthven
	Allow using my new xmlVer attribute in the XML.
	If using my XML patches against MythTV show the disk space details.

2007-12-07	Andrew Ruthven
	Better suppress warnings from the MythTV Perl API if we can't access
	  the database.

2007-12-06	Andrew Ruthven
	Be more paranoid about handling error conditions in the
	  substitution code.
	Handle the case where there is no next recording scheduled
	  in a nicer fashion.

	Release version 0.5.3.

2007-12-02	Andrew Ruthven
	Allow sending emails only if there is low disk space or recording
	  conflicts. (0.6)

2007-11-28	Andrew Ruthven
	Be more wary about processing what the backend has sent us.
	This includes cleaning up some invalid UTF-8 characters.
	Add support for reading XML from a file.
	Be a bit more forgiving on the XML we're receiving.

	Release version 0.5.2.

	Show how much disk space is used - currently only total. (0.6)
	Rename "Time till next recording" to "Next Recording In". (0.6)
	Put all the one liners together in the output and make them line
	  up nicely. (0.6)

2007-11-23	Andrew Ruthven
	Don't set the background when changing the colour.
	Make the new debconf prompt lintian clean and say how to specify
	  multiple email addresses.

	Release version 0.5.1.

2007-11-21	Andrew Ruthven
	Now the results can be sent via email with the subject set correctly.
	  With an option to only send out email if schedule conflicts are
	  detected.

	Release version 0.5.

2007-11-20	Andrew Ruthven
	Allow suppressing blocks of output.
	Add a semi-FAQ.

2007-11-18	Andrew Ruthven
	Add support for showing the version of the script.
	Now we show any schedule conflicts, or if the MythTV Perl API isn't
	  usable, a warning.  (This is because we need to be able to read
	  the mysql.txt file and connect to the database to use the API, ick.)
	Display the amount of time until the next recording.

2007-11-17	Andrew Ruthven
	Add support for printing colour in the encoder status display.

2007-11-16	Francois Marier
	Many fixes to the Debian packaging to make lintian happy.
	Fixes to the Makefile for supporting Debian sponsoring.
	Check if the initscript exists before running it

2007-11-16	Andrew Ruthven
  Add additional encoder states and logic for displaying the
	connected state of encoders.

	Release version 0.3.

	Changes to the Makefile required by new packaging.
	Tweaks to the Debian packaging.
	Optionally display programs description and/or subtitle.

	Release version 0.4.

2007-11-15	Andrew Ruthven
	Fix up publishing the tar ball.

	Release version 0.2.

2007-11-05	Andrew Ruthven
	Add an example when recording.
	Work the Debian packaging

2007-10-31	Andrew Ruthven
	Expand out MOTD in the documentation.
	Add a section to the perldoc which describes what is in the output.

2007-10-27	Andrew Ruthven
	Debian packaging:
	 - Convert the perldoc to a man page, and install it.
	 - Ignore the build files/directories.
	Remove the bit about how to get the GPLv2 from the perldoc.
	Add perldoc.

	Release version 0.1.

2007-10-22	Andrew Ruthven
	Add Debian directory
	Add a README file.
	Check for the Debian settings and use them.
	Add license.
	Update the MOTD.
	Rename to what we'll use when installing it.
	Pull out the XML generated timestamp and allow one line output.
