#!/bin/sh
# Copyright (c) 2007-2025 Andrew Ruthven <andrew@etc.gen.nz>
# This code is hereby licensed for public consumption under the GNU GPL v3.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
# Run mythtv-status to perform a health check of the MythTV system.

# Debian style
[ -r /etc/default/mythtv-status ]   && . /etc/default/mythtv-status
# Fedora style
[ -r /etc/sysconfig/mythtv-status ] && . /etc/sysconfig/mythtv-status

if [ "$EMAIL" = "none" ]
then
  exit
fi

@@BINDIR@@/mythtv-status --host "${HOST:-localhost}" --email "${EMAIL:-root}" "${EMAIL_ARGS:---email-only-on-alert}"

# Keep things happy by always having a return code of 0.
exit 0
