#!/bin/sh
# Copyright (c) 2008-2025 Andrew Ruthven <andrew@etc.gen.nz>
# This code is hereby licensed for public consumption under the GNU GPL v3.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

# Return an exit code of 0 if the MythTV backend is currently recording
# a show or will be recording a show soon (in the next hour).

# Debian style
[ -r /etc/default/mythtv-status ]   && . /etc/default/mythtv-status
# Fedora style
[ -r /etc/sysconfig/mythtv-status ] && . /etc/sysconfig/mythtv-status

if [ "x$1" != "x" ]
then
  ARGS="--recording-in-warn $1"
else
  ARGS="--recording-in-warn 600"
fi

@@BINDIR@@/mythtv-status -h "${HOST:-localhost}" --return-code-only \
  --recording-now --next-recording $ARGS

if [ $? -eq 1 ]
then
  exit 0
else
  exit 1
fi
