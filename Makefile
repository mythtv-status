#!/usr/bin/make -f

BINDIR?=/usr/bin
LIBEXECDIR?=/usr/libexec/mythtv-status
MANDIR?=/usr/share/man
MOLLY_GUARDDIR?=/etc/molly-guard/run.d
SBINDIR?=/usr/sbin
SYSTEMDDIR?=/usr/lib/systemd/system
VARLIBDIR?=/var/lib/mythtv-status
UPDATE_MOTDDIR?=/etc/update-motd.d

FILES_TO_SUBSTITUTE=$(wildcard */*.in)
SUBSTITUTED_FILES=$(shell echo $(FILES_TO_SUBSTITUTE) | sed 's/\.in//g')

package=mythtv-status

# Only try and find a git tag if we're running in a git checkout, and allow
# for the version to be overridden.
ifneq ("$(wildcard .git)", "")
ifeq ($(version),)
version=$(shell git tag -l | grep '^[0-9]' | sort -t"." -k 1,1 -k 2,2 -k 3,3 -n | tail -1)
endif
endif

tarball=../build/tarball/$(package)-$(version).tar.gz
tarball_dir=../$(package)_tarballs

RELEASE_FILES=$(tarball)

TEST_VERBOSE=0
TEST_FILES=t/*.t

# No default action.
all:

dist: test release

release: $(RELEASE_FILES)

$(tarball):
	@mkdir -p $(@D)
	@git archive --format=tar --prefix=$(package)-$(version)/ $(version) `git ls-tree --name-only $(version) | egrep -v "(.gitignore|testing)"` | gzip > $(tarball)
	@gpg -sb --armour $(tarball)

$(SUBSTITUTED_FILES): $(FILES_TO_SUBSTITUTE)
	sed -e 's,@@BINDIR@@,$(BINDIR),g;s,@@VARLIBDIR@@,$(VARLIBDIR),g;s,@@LIBEXECDIR@@,$(LIBEXECDIR),g;s,@@SBINDIR@@,$(SBINDIR),g' $@.in > $@

publish: $(RELEASE_FILES)
	@cp $(tarball) $(tarball_dir)
	@chmod o+r $(tarball_dir)/*-$(version).*
	@ln -sf `basename $(tarball)` $(tarball_dir)/$(package)-latest.tar.gz

build: $(SUBSTITUTED_FILES)
	@touch build.stamp

install: $(SUBSTITUTED_FILES)
	install -d $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(SBINDIR)
	install -d $(DESTDIR)$(LIBEXECDIR)
	install -d $(DESTDIR)$(MANDIR)/man1
	install -d $(DESTDIR)$(MANDIR)/man8
	install -d $(DESTDIR)$(SYSTEMDDIR)
	install -m 775 bin/mythtv-status $(DESTDIR)$(BINDIR)
	install -m 775 bin/mythtv_recording_now $(DESTDIR)$(BINDIR)
	install -m 775 bin/mythtv_recording_soon $(DESTDIR)$(BINDIR)
	install -m 775 bin/mythtv-update-motd $(DESTDIR)$(SBINDIR)
	install -m 775 bin/health-check $(DESTDIR)$(LIBEXECDIR)
	pod2man bin/mythtv-status > $(DESTDIR)$(MANDIR)/man1/mythtv-status.1
	install -m 644 man/mythtv-update-motd.8 $(DESTDIR)$(MANDIR)/man8
	install -m 644 man/mythtv_recording_now.1 $(DESTDIR)$(MANDIR)/man1
	install -m 644 man/mythtv_recording_soon.1 $(DESTDIR)$(MANDIR)/man1
	install -m 644 systemd/*.service $(DESTDIR)$(SYSTEMDDIR)
	install -m 644 systemd/*.timer $(DESTDIR)$(SYSTEMDDIR)

        # Only install these files if the directories already exist.
	if [ -d $(DESTDIR)$(MOLLY_GUARDDIR) ]; then \
	    install -m 775 molly-guard/40-mythtv-recording-soon $(DESTDIR)$(MOLLY_GUARDDIR); \
	fi
	if [ -d $(DESTDIR)$(UPDATE_MOTDDIR) ]; then \
	    install -m 775 update-motd.d/* $(DESTDIR)$(UPDATE_MOTDDIR); \
	fi

test:
	PERL_DL_NONLAZY=1 perl "-MExtUtils::Command::MM" "-e" "test_harness($(TEST_VERBOSE))" $(TEST_FILES)

clean:
	@rm -rf build.stamp $(SUBSTITUTED_FILES)

.PHONY: release clean install test dist build
